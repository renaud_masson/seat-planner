import os
import sys
import math
import random

def generateAffinityMatrix(nbGuests):
  friendShip = [[0 for j in range(nbGuests)] for i in range(nbGuests)]
  for i in range(nbGuests):
    for j in range(i):
      s = random.randrange(1,5)
      if s == 1:
        friendShip[i][j] = random.randrange(0,5)
      else:
        friendShip[i][j] = 2
      friendShip[j][i] = friendShip[i][j]
  return friendShip
 

def main():
  args = sys.argv[1:]
  affinity = generateAffinityMatrix(int(args[0]))
  tables = [int(args[1]) for i in range(int(args[0])/int(args[1]))]
  fout = open(args[2],'w')
  for t in tables[:-1]:
    fout.write(str(t)+' ')
  fout.write(str(tables[-1])+'\n')
  for d in affinity:
    for dd in d[:-1]:
      fout.write(str(dd)+' ')
    fout.write(str(d[-1])+'\n')
  fout.close()

if __name__ == "__main__":
  main()

# vim:set encoding=utf-8 
# vim:set fileencoding=utf-8

