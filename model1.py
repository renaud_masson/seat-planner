import random
import os
import sys
import math
import parser
from pulp import *



class Model1:

  def __init__(self, friendShip, tableSizes):
    self.friendShip = friendShip
    self.tableSizes = tableSizes
    self.model = LpProblem("Table Assignment Model 1", sense=LpMaximize)
  
  def createVariables(self):
    # First we create the variables indicating if a guest is seated at a table.
    self.x = []
    for g in range(len(self.friendShip)):
      self.x.append([])
      for t in range(len(self.tableSizes)):
        self.x[-1].append(LpVariable("x_"+str(g)+"_"+str(t),lowBound=0,upBound=1,cat=LpInteger))
    # Then we create the variables indicating if two guests seat at the same table.
    self.y = []
    for g in range(len(self.friendShip)):
      self.y.append([])
      for gg in range(len(self.friendShip)):
        self.y[-1].append([])
        for t in range(len(self.tableSizes)):
          self.y[-1][-1].append(LpVariable("y_"+str(g)+"_"+str(gg)+"_"+str(t),lowBound=0,upBound=1,cat=LpInteger))

  def createObjectiveFunction(self):
    self.model += lpSum([lpSum([lpSum([self.y[g][gg][t]*self.friendShip[g][gg] for t in range(len(self.tableSizes))]) for gg in range(len(self.friendShip))]) for g in range(len(self.friendShip))])

  def createConstraintSeatedOnce(self):
    for g in range(len(self.friendShip)):
      cst = lpSum([self.x[g][t] for t in range(len(self.tableSizes))]) == 1
      cst.name = "seatedOnce_"+str(g)
      self.model += cst

  def createConstraintOneSeatPerGuest(self):
    for t in range(len(self.tableSizes)):
      cst = lpSum([self.x[g][t] for g in range(len(self.friendShip))]) == self.tableSizes[t]
      cst.name = "oneSeatPerGuest_"+str(t)
      self.model += cst

  def createConstaintsTogetherIfSameTable(self):
    for g in range(len(self.friendShip)):
      for gg in range(len(self.friendShip)):
        for t in range(len(self.tableSizes)):
          cst = self.y[g][gg][t] <= self.x[g][t]
          cst.name = "together_"+str(g)+"_"+str(gg)+"_"+str(t)+"_1"
          self.model += cst
          cst2 = self.y[g][gg][t] <= self.x[gg][t]
          cst2.name = "together_"+str(g)+"_"+str(gg)+"_"+str(t)+"_2"
          self.model += cst2


  def generate(self):
    self.createVariables()
    self.createObjectiveFunction()
    self.createConstraintSeatedOnce()
    self.createConstraintOneSeatPerGuest()
    self.createConstaintsTogetherIfSameTable()

  def solve(self):
    status = self.model.solve(CPLEX(msg = 1))
    return status
      


def main():
  args = sys.argv[1:]
  sizes, friendShip= parser.parseInstance(args[0])
  mod = Model1(friendShip,sizes)
  mod.generate()
  print mod.solve()
  for i in range(len(friendShip)):
    for j in range(len(friendShip)):
      for t in range(len(sizes)):
        if mod.y[i][j][t].value() >= 0.5:
          print i,j,t

if __name__ == "__main__":
  main()

# vim:set encoding=utf-8 
# vim:set fileencoding=utf-8

