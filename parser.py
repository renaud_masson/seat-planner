import os
import sys
import math

def parseInstance(path):
  f = open(path,'r')
  lines = f.readlines()
  l = lines[0][:-1]
  tables = [int(a) for a in l.split(' ')]
  affinity = []
  for ll in lines[1:]:
    l = ll[:-1]
    affinity.append([int(a) for a in l.split(' ')])
  return (tables,affinity)

def main():
  args = sys.argv[1:]

if __name__ == "__main__":
  main()

# vim:set encoding=utf-8 
# vim:set fileencoding=utf-8

