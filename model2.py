import random
import os
import sys
import math
import parser
from pulp import *



class Model2:

  def __init__(self, friendShip, tableSizes):
    self.friendShip = friendShip
    self.tableSizes = tableSizes
    self.model = LpProblem("Table Assignment Model 2", sense=LpMaximize)
  
  def createVariables(self):
    # First we create the variables indicating if two guests are seated at the same table
    self.x = []
    for g in range(len(self.friendShip)):
      self.x.append([])
      for gg in range(len(self.friendShip)):
        if not gg == g:
          self.x[-1].append(LpVariable("x_"+str(g)+"_"+str(gg),lowBound=0,upBound=1,cat=LpInteger))
        else:
          self.x[-1].append(LpVariable("x_"+str(g)+"_"+str(gg),lowBound=0,upBound=0))


  def createObjectiveFunction(self):
    self.model += lpSum([lpSum([self.x[g][gg]*self.friendShip[g][gg] for gg in range(len(self.friendShip))]) for g in range(len(self.friendShip))]) 

  def createConstraintSeatedOnce(self):
    for g in range(len(self.friendShip)):
      cst = lpSum([self.x[g][gg] for gg in range(len(self.friendShip))]) == self.tableSizes[0] - 1
      cst.name = "seatedOnce_"+str(g)
      self.model += cst

  def createConstaintsTogetherIfSameTable(self):
    for g in range(len(self.friendShip)):
      for gg in range(len(self.friendShip)):
        cstA = self.x[g][gg] == self.x[gg][g]
        cstA.name = "symmetry_"+str(g)+"_"+str(gg)
        self.model += cstA
        for ggg in range(len(self.friendShip)):
          if not (ggg == gg or ggg == g):
            cst = self.x[g][ggg] >= self.x[g][gg] + self.x[gg][ggg] - 1.0
            cst.name = "together_"+str(g)+"_"+str(gg)+"_"+str(ggg)
            self.model += cst

  def generate(self):
    self.createVariables()
    self.createObjectiveFunction()
    self.createConstraintSeatedOnce()
    self.createConstaintsTogetherIfSameTable()

  def solve(self):
    status = self.model.solve(GLPK(msg = 1))
    return status
      


def main():
  args = sys.argv[1:]
  sizes, friendShip = parser.parseInstance(args[0]) 
  sizesOk = True
  for i in range(len(sizes)-1):
    if not sizes[i] == sizes[i+1]:
      sizesOk = False
  if sizesOk: 
    mod = Model2(friendShip,sizes)
    mod.generate()
    print mod.solve()
    for i in range(len(friendShip)):
      for j in range(len(friendShip)):
        if mod.x[i][j].value() >= 0.5:
          print i,j
  else:
    print 'This model cannot handle instances with heterogeneous table sizes', sizes

if __name__ == "__main__":
  main()

# vim:set encoding=utf-8 
# vim:set fileencoding=utf-8

